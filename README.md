<div align="center">
  <img src="https://gitlab.com/kkrishguptaa/csv2json/-/raw/main/assets/logo.png" height="100px" width="100px" />
  <br />
  <h1>CSV2JSON</h1>
  <p>Minimalistic CSV2JSON Converter 🎨</p>
  <p><a href="https://kkrishguptaa-learns.gitlab.io/csv2json"><img src="https://img.shields.io/badge/View%20Deployed-2965F1?style=for-the-badge" alt="View Deployed" /></a></p>
</div>

## 📸 Screenshots


| Desktop | Mobile |
| --- | --- |
| ![Screenshot of the application, the page includes a heading "CSV2JSON", a textbox for input and a block of output](https://gitlab.com/kkrishguptaa/csv2json/-/raw/main/assets/screenshots/desktop.png) |   ![Screenshot of CSV2JSON On A Mobile Device To Showcase That It's Responsive](https://gitlab.com/kkrishguptaa/csv2json/-/raw/main/assets/screenshots/mobile.png)  |

## 💡 Origin

Details can be found => [#6](https://github.com/kkrishguptaa/learning/issues/6)
